Welcome to GPCRdb's documentation
=================================

GPCRdb contains data, diagrams and web tools for G protein-coupled receptors (GPCRs).
Users can browse all GPCR crystal structures and the largest collections of receptor mutants.
Diagrams can be produced and downloaded to illustrate receptor residues (snake-plot and helix box diagrams) and
relationships (phylogenetic trees). Reference (crystal) structure-based sequence alignments take into account helix
bulges and constrictions, display statistics of amino acid conservation and have been assigned generic
residue numbering for equivalent residues in different receptors

The `source code`_ and `source data`_ are freely available on `Bitbucket`_.

.. _source code: http://bitbucket.org/gpcr/protwis.git
.. _source data: http://bitbucket.org/gpcr/gpcrdb_data.git
.. _Bitbucket: http://bitbucket.org

The documentation in organized into three sections:

* :ref:`user-docs`
* :ref:`dev-docs`
* :ref:`about-docs`

.. _user-docs:

.. toctree::
    :maxdepth: 2
    :caption: User documentation

    receptors
    sequences
    structures
    mutants
    sites
    generic_numbering

.. _dev-docs:

.. toctree::
    :maxdepth: 2
    :caption: Developer documentation

    web_services
    contributing
    local_installation
    coding_style
    git_workflow

.. _about-docs:

.. toctree::
    :maxdepth: 2
    :caption: About GPCRdb

    about
    contact
    citing
    acknowledgements
    legal_notice
    meetings
    linking
    external_servers
